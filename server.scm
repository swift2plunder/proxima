(use-modules (fibers web server)
       (web request)
             (web response)
             (web uri)
       (sxml simple)
       (ice-9 match))

;; The get-* namespace is our utility functions and aliases
;; that we'll use in our router and handlers
(define (get-route request)
  (split-and-decode-uri-path(uri-path (request-uri request))))

(define (get-method request)
  (request-method request))

;; Templates - These will probably end up in a separate file
(define (templatize title body)
  `(html (head (title ,title))
         (body ,@body)))

;; The handle-* namespace will contain all of our handler macros
(define (handler-GET request body)
  (values '((content-type . (text/plain))) "Hello"))

(define (handler-POST request body)
  (prepare-sxml-response
   `((h1 "hello world!")
     (table
      (tr (th "header") (th "value"))
      ,@(map (lambda (pair)
               `(tr (td (tt ,(with-output-to-string
                               (lambda () (display (car pair))))))
                    (td (tt ,(with-output-to-string
                               (lambda ()
                                 (write (cdr pair))))))))
             (request-headers request))))))

(define (handler-404 request body)
  (values (build-response #:code 404)
          (string-append "Resource not found: "
                         (uri->string (request-uri request)))))

(define (handler-405 request body)
  (values (build-response #:code 405)
          (string-append "Method not allowed: "
                         (uri->string (get-method request)))))

(define (handler-501 request body)
     (values (build-response #:code 501)
          (string-append "Method not implemented: "
                         (uri->string (get-method request)))))

;; The prepare-* namespace is helpers for preparing responses
(define* (prepare-sxml-response #:optional body #:key
                  (status 200)
                  (title "Greetings")
                  (doctype "<!DOCTYPE html>\n")
                  (content-type-params '((charset . "utf-8")))
                  (content-type 'text/html)
                  (extra-headers '())
                  (sxml (and body (templatize title body))))
  (values (build-response
           #:code status
           #:headers `((content-type
                        . (,content-type ,@content-type-params))
                       ,@extra-headers))
          (lambda (port)
            (if sxml
                (begin
                  (if doctype (display doctype port))
                  (sxml->xml sxml port))))))


;; The router function is our routing table
;; It should be as easy to read as possible
(define (router request body)
    (match (cons (request-method request) (get-route request))
     ('(GET) (handler-GET request body))))



(run-server router)

